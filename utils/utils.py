import tensorflow as tf
from keras import backend as K
import numpy as np

def recall_m(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
        recall = true_positives / (possible_positives + K.epsilon())
        return recall

def precision_m(y_true, y_pred):
        true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
        predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
        precision = true_positives / (predicted_positives + K.epsilon())
        return precision

def f1_m(y_true, y_pred):
    precision = precision_m(y_true, y_pred)
    recall = recall_m(y_true, y_pred)
    return 2*((precision*recall)/(precision+recall+K.epsilon()))

# What is it used for?
def label_smoothing(inputs, epsilon=0.1):
    """
    Applies label smoothing. See 5.4 and https://arxiv.org/abs/1512.00567.
    inputs: 3d tensor. [N, T, V], where V is the number of vocabulary.
    epsilon: Smoothing rate.
    """
    V = inputs.get_shape().as_list()[-1]  # number of channels
    return ((1 - epsilon) * inputs) + (epsilon / V)


def get_batch_data(X, Y=None):
    # calc total batch count
    # batch_size = len(X) // num_batch
    batch_size = 32

    # Convert to tensor
    X = tf.convert_to_tensor(X, tf.float32)
    if Y.any():
        Y = tf.convert_to_tensor(Y, tf.float32)
        # Y = label_smoothing(Y)

        dataset = tf.data.Dataset.from_tensor_slices((X, Y))
        batched_dataset = dataset.shuffle(1).batch(batch_size)
    else:
        dataset = tf.data.Dataset.from_tensor_slices((X))
        batched_dataset = dataset.shuffle(1).batch(batch_size)
    return batched_dataset

def get_angles(pos, i, d_model):
  angle_rates = 1 / np.power(10000, (2 * (i//2)) / np.float32(d_model))
  return pos * angle_rates


def positional_encoding(position, d_model):
  angle_rads = get_angles(np.arange(position)[:, np.newaxis],
                          np.arange(d_model)[np.newaxis, :],
                          d_model)
  

  angle_rads[:, 0::2] = np.sin(angle_rads[:, 0::2])
  

  angle_rads[:, 1::2] = np.cos(angle_rads[:, 1::2])
    
  pos_encoding = angle_rads[np.newaxis, ...]
    
  return tf.cast(pos_encoding, dtype=tf.float32)

