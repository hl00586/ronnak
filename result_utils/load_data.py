from imblearn.over_sampling import SMOTE
from imblearn.under_sampling import NeighbourhoodCleaningRule
import numpy as  np
import tensorflow as tf
from sklearn import model_selection
# Load the data

def load_data(data_augumentation=None, normalisation=False):
  train_x = np.load('data/train_raw.npy').astype('float32')
  train_y = np.load('data/train_label.npy', allow_pickle=True).astype('float32')
  train_y = tf.keras.utils.to_categorical(train_y)

  if normalisation:
    train_x = (train_x - np.mean(train_x)) / np.std(train_x)

  X_train, X_test, y_train, y_test = model_selection.train_test_split(train_x,train_y, stratify=train_y, 
                                                                    test_size=0.4, random_state=13)
  
  if data_augumentation is not None:
    if data_augumentation == 'Up':
      smote = SMOTE()
    elif data_augumentation == 'Down':
      smote = NeighbourhoodCleaningRule()

    X_train = X_train.reshape(X_train.shape[0],-1)
    X_train, y_train = smote.fit_sample(X_train, np.argmax(y_train,axis=1))

    X_train = X_train.reshape(X_train.shape[0],48,76)
    y_train = tf.keras.utils.to_categorical(y_train)

  return X_train,y_train, X_test, y_test