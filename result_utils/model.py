from keras.layers import Dense, Input, Flatten, Lambda, LSTM
from keras.models import Model
from keras_pos_embd import TrigPosEmbedding
from layers.layers_keras import *
from layers.layers import Probability_CLF_Mul
from result_utils.losses import *  
from layers.generator import *

def model_generator(params, retrun_generator=False):
    use_lstm = params['use_lstm']
    print('model information'.center(50,'-'))
    for key,value in params.items():
        print('{0:<10} --- '.format(key),value)
    print('End'.center(50,'-'))
    
    inputs = Input(shape=(48,76))
    hidden = inputs
    ''' Rational Block'''
    for i in range(params['Rational']):
        important_rate, crossentropy = Generator_time()(hidden)
        keras.backend.stop_gradient(important_rate)
        hidden = LSTM(76,return_sequences=use_lstm)(inputs,mask=important_rate)
        if retrun_generator:
            gener_output = hidden

    ''' Position Embedding'''
    if params['PE'] and use_lstm:
        hidden = TrigPosEmbedding(output_dim=76,mode=TrigPosEmbedding.MODE_ADD)(hidden)

    ''' Attention Block'''
    for i in range(params['Attention']):
        if params['Rational']:
            hidden = attention_block(hidden,important_rate=important_rate,multiple_weights=use_lstm)    
        else:
            hidden = attention_block(hidden,multiple_weights=use_lstm)

    if not use_lstm:
        hidden = Flatten()(hidden)

    ''' Residual Block'''
    for i in range(params['Residual']):
        hidden = residual_block(hidden,48*76)


    ''' Hidden Layers '''
    for i in range(params['Hidden']):
        if use_lstm:
            hidden = LSTM(200,activation='relu')(hidden, mask=important_rate)
        else:
            hidden = Dense(200,activation='relu')(hidden)


    if params['PNN']:
        outputs = Probability_CLF_Mul(2)(hidden)
    else:
        outputs = Dense(2,activation='softmax')(hidden)
        
    model = Model(inputs = inputs,outputs = outputs)


    def get_loss_func():
        name = params['Loss']
        if params['Rational']:
            if name == 'focal':
                return integrated_loss(crossentropy, important_rate)
            elif name == 'gener':
                return gener_loss(crossentropy, important_rate)
            elif name == 'crossen':
                return 'categorical_crossentropy'
            elif name == 'mse':
                return 'mse'
        else:
            if name == 'focal':
                return binary_focal_loss()
            elif name == 'gener':
                return None
            elif name == 'crossen':
                return 'categorical_crossentropy'
            elif name == 'mse':
                return 'mse'


    model.compile(loss=get_loss_func(),optimizer='adam',metrics=['accuracy'])

    if retrun_generator:
        model_gen = Model(inputs = inputs, outputs= gener_output)
        model_time = Model(inputs = inputs, outputs= important_rate)
        return model, model_gen, model_time

    return model
    
















