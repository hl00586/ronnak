import tensorflow as tf

# Get the rationals
class Generator(tf.keras.layers.Layer):
    def __init__(self, dff=200, rate=0.3):
        super(Generator, self).__init__()
        #attention
        self.attention = attention_layer()
        self.attention_dropout = tf.keras.layers.Dropout(rate)
        self.attention_norm = tf.keras.layers.LayerNormalization(epsilon=1e-6)
        #fully connected
        self.dense_1 = tf.keras.layers.Dense(dff, activation='relu')
        self.dense_2 = tf.keras.layers.Dense(1)
        self.ffn_dropout = tf.keras.layers.Dropout(rate)
        self.ffn_norm = tf.keras.layers.LayerNormalization(epsilon=1e-6)
        
    def call(self, x, training=True):
        # x shape == (batch_size, timestep, features)
        
        # attn_output shape == (batch_size, timestep, 1)
        attn_output, _ = self.attention(x, x, x) 
        attn_output = self.attention_dropout(attn_output, training=training)
        # (first)out1 shape == (batch_size, timestep, features)
        #out1 = self.attention_norm(x + attn_output) 
        
        # (second)out1 shape == (batch_size, timestep, 1)
        out1 = self.attention_norm(attn_output)
        
        # (first)ffn_output shape == (batch_size, timestep, features)
        # (second)ffn_output shape == (batch_size, timestep, 1)
        ffn_output = self.dense_2(self.dense_1(out1)) 
        ffn_output = self.ffn_dropout(ffn_output, training=training)
        out2 = self.ffn_norm(out1 + ffn_output)
        
        # zProbs and self.zPreds shape == (batch_size, timestep, 1)
        #(first)zProbs = tf.keras.activations.sigmoid(attn_output)
        zProbs = tf.keras.activations.sigmoid(out2)
        # sigmoid sampling approximation 
        #zPreds = 1.0 / (1.0 + tf.exp(-60.0*(zProbs-0.5))) # sigmoid to simulate rounding
        
        binomial = tfp.distributions.Binomial(total_count=1, probs=zProbs)
        samples = binomial.sample()
        z = tf.dtypes.cast(tf.less(samples, zProbs), tf.float32)
        
        crossEntropy = -1.0 * (((z * tf.math.log(zProbs + 0.001)) + ((1 - z) * tf.math.log(1 - zProbs + 0.001))))
        zPreds = tf.stop_gradient(z)
        
        #(first)return out2, z, zPreds, crossEntropy
        return z, zProbs, crossEntropy