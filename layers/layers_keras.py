import tensorflow as tf
from keras.layers import Layer, Dense, Dropout, TimeDistributed, LSTM
import keras
from keras_layer_normalization import LayerNormalization
import keras.backend as K
import numpy as np

def attention_block(inputs,important_rate=None, multiple_weights=True):
    attention = Multi_head_time_attention(multiple_weights=multiple_weights)(inputs=inputs, important_rate=important_rate)
    attention = Dropout(0.5)(attention)
    add_merge = keras.layers.Add()([inputs, attention])
    return LayerNormalization()(add_merge)

def residual_block(inputs,output_dim):
    hidden = Dense(output_dim,activation='relu')(inputs)
    hidden = Dense(output_dim)(hidden)
    hidden = Dropout(0.5)(hidden)
    add_merge = keras.layers.Add()([inputs, hidden])
    return LayerNormalization()(add_merge)


class Multi_head_time_attention(Layer):
    """docstring for Multi_head_time_attention"""

    def __init__(self, multiple_weights=False, activation=None, **kwargs):
        super(Multi_head_time_attention, self).__init__(**kwargs)
        self.activation = activation
        self.multiple_weights = multiple_weights

    def build(self, input_shape):
        """
        input shape: N * T * F

        Weight Matrix:
            T * F * Output_dim
        """
        if self.multiple_weights:
          self.WQ = LSTM(76,return_sequences=True)  #TimeDistributed(Dense(input_shape[2])) 
          self.WK = LSTM(76,return_sequences=True)  #TimeDistributed(Dense(input_shape[2])) 
          self.WV = LSTM(76,return_sequences=True)  #TimeDistributed(Dense(input_shape[2])) 
        else:
          self.WQ = Dense(input_shape[-1])
          self.WK = Dense(input_shape[-1])
          self.WV = Dense(input_shape[-1])

        super(Multi_head_time_attention, self).build(input_shape)

    def call(self, inputs, important_rate=None,training=None):
        if self.multiple_weights:
          q = self.WQ(inputs,mask=important_rate)
          k = self.WK(inputs,mask=important_rate)
          v = self.WV(inputs,mask=important_rate)
          d_k = q.shape.as_list()[2]
          weights = K.batch_dot(q,  k, axes=[2, 2])
          normalized_weights = K.softmax(weights / np.sqrt(d_k))
          output = K.batch_dot(normalized_weights, v)
        else:
          q = self.WQ(inputs)
          k = self.WK(inputs)
          v = self.WV(inputs)
          d_k = q.shape.as_list()[1]

          weights = K.dot(q,  K.transpose(k))
          normalized_weights = K.softmax(weights / np.sqrt(d_k))
          output = K.dot(normalized_weights, v)
        
        return output


    def compute_output_shape(self, input_shape):
        return input_shape

class PositionwiseFeedForward():
  def __init__(self, d_hid, d_inner_hid, dropout=0.1):
    self.w_1 = Conv1D(d_inner_hid, 1, activation='relu')
    self.w_2 = Conv1D(d_hid, 1)
    self.layer_norm = LayerNormalization()
    self.dropout = Dropout(dropout)
  def __call__(self, x):
    output = self.w_1(x) 
    output = self.w_2(output)
    output = self.dropout(output)
    output = Add()([output, x])
    return self.layer_norm(output)

'''
class Multi_head_time_attention(Layer):
    """docstring for Multi_head_time_attention"""

    def __init__(self, activation=None, **kwargs):
        super(Multi_head_time_attention, self).__init__(**kwargs)
        self.params = {}
        self.activation = activation

    def build(self, input_shape):
        """
        input shape: N * T * F

        Weight Matrix:
            T * F * Output_dim
        """
        for i in range(input_shape[1]):
            self.params[i] = {}
            self.params[i]['WQ'] = self.add_weight(name='query_weights%d' % i, shape=(input_shape[2], input_shape[2]),
                                                   initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05,
                                                                                               seed=None),
                                                   trainable=True)
            self.params[i]['WK'] = self.add_weight(name='key_weights%d' % i, shape=(input_shape[2], input_shape[2]),
                                                   initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05,
                                                                                               seed=None),
                                                   trainable=True)
            self.params[i]['WV'] = self.add_weight(name='value_weights%d' % i, shape=(input_shape[2], input_shape[2]),
                                                   initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05,
                                                                                               seed=None),
                                                   trainable=True)

        super(Multi_head_time_attention, self).build(input_shape)

    def call(self, inputs, training=None):
        outputs = []
        for i in range(inputs.shape[1]):
            Q = keras.backend.dot(inputs[:, i, :], self.params[i]['WQ'])
            K = keras.backend.dot(inputs[:, i, :], self.params[i]['WK'])
            V = keras.backend.dot(inputs[:, i, :], self.params[i]['WV'])
            dk = tf.cast(tf.shape(inputs)[-1], tf.float32)
            attention_logits = tf.nn.softmax(tf.matmul(Q, K, transpose_b=True) / tf.math.sqrt(dk))
            outputs.append(keras.backend.dot(attention_logits, V))  # SHape = N * Output dim

        return tf.stack(tf.unstack(outputs, axis=0), axis=1)

    def compute_output_shape(self, input_shape):
        return input_shape
'''









































