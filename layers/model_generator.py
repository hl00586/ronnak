from keras.layers import Dense, Input, Flatten, Lambda
from keras.models import Model
from keras_pos_embd import TrigPosEmbedding


def model_generator(combin):
    print('model information'.center(50,'-'))
    for idx,key in enumerate(params.keys()):
        print('{0:<10} --- '.format(key),combin[idx])
    print('End'.center(50,'-'))
    
    rational, attention, residual, PNN,focal_loss = combin
    inputs = Input(shape=(48,76))
    hidden = inputs
    ''' Rational Block'''
    if rational:
        hidden, crossentropy = Generator_time()(hidden)
        keras.backend.stop_gradient(important_rate)


    ''' Position Embedding'''
    #hidden = keras.layers.Add()([positional_encoding(48,76)[:, :48, :],inputs])
    hidden = TrigPosEmbedding(output_dim=76,mode=TrigPosEmbedding.MODE_ADD)(hidden)

    ''' Attention Block'''
    if attention:
        hidden = attention_block(hidden,multiple_weights=MULTIPLE_WEIGHTS)

    hidden = Flatten()(hidden)

    ''' Residual Block'''
    if residual:
        hidden = residual_block(hidden,48*76)


    hidden = Dense(200,activation='relu')(hidden)

    if PNN:
        outputs = Probability_CLF_Mul(2)(hidden)
    else:
        outputs = Dense(2,activation='softmax')(hidden)
        
    model = Model(inputs = inputs,outputs = outputs)
    #model.compile(loss=gener_loss(gen_loss,important_rate),optimizer='adam',metrics=['accuracy'])
    if rational:
        model.compile(loss=gener_loss(crossentropy,important_rate),optimizer='adam',metrics=['accuracy'])
    elif focal_loss:
        model.compile(loss=binary_focal_loss(),optimizer='adam',metrics=['accuracy'])
    elif PNN:
        model.compile(loss='mse',optimizer='adam',metrics=['accuracy'])
    else:
        model.compile(loss='categorical_crossentropy',optimizer='adam',metrics=['accuracy'])
    return model
    