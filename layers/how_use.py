self.output_layer = output_layer = Z_Layer(h_final.get_shape()[2], initializer = initializer)
                
# sample a which words should be kept
zpred = output_layer.sample_all(h_final)


self.zpredsum = tf.reduce_sum(zpred)

# z itself should not be updated
zpred = tf.stop_gradient(zpred)

# get the probabilities and log loss
with tf.name_scope('zlayer_forward_pass'):
    probs, logits = output_layer.forward_all(h_concat, zpred)

with tf.name_scope('sigmoid_cross_entropy'):
    
    # this error function is the binary cross entropy rewritten
    # in terms of softplus, should be more numerically stable
    
    logpz = (-logits*(1-zpred)-tf.nn.softplus(-logits)) * masks
    
logpz = self.logpz = tf.reshape(logpz,tf.shape(x),
                                name = 'reshape_logpz')
probs = self.probs = tf.reshape(probs, tf.shape(x),
                                name = 'probs_reshape')
