import tensorflow as tf
from tensorflow import keras

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer
        


class Attention_layer(tf.keras.layers.Layer):
    def __init__(self, output_dim):
        self.output_dim = output_dim
        super(Attention_layer, self).__init__()

    def build(self, input_shape):

        self.WQ = self.add_weight(name='query_weights', shape=(input_shape[1], self.output_dim), initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=None),
                                  trainable=True)
        self.WK = self.add_weight(name='key_weights', shape=(input_shape[1], self.output_dim), initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=None),
                                  trainable=True)
        self.WV = self.add_weight(name='value_weights', shape=(input_shape[1], self.output_dim), initializer=keras.initializers.RandomNormal(mean=0.0, stddev=0.05, seed=None),
                                  trainable=True)

        super(Attention_layer, self).build(input_shape)

    def call(self, X):
        Q = tf.matmul(X, self.WQ)
        K = tf.matmul(X, self.WK)
        V = tf.matmul(X, self.WV)

        dk = tf.cast(tf.shape(X)[-1], tf.float32)

        attention_logits = tf.nn.softmax(tf.matmul(Q, K, transpose_b=True) / tf.math.sqrt(dk))
        outputs = tf.matmul(attention_logits, V)

        return outputs

    def compute_output_shape(self, input_shape):
        return (input_shape[0], self.output_dim)

def mulhead_self_att(inputs,
                     head_num=4,
                     add_nor=True,
                     activation='linear'):
    x=inputs
    unit_num=int(x._keras_shape[-1])
    x=Self_Attention(head_num,int(unit_num/head_num))([x,x,x])
    if add_nor==True:
        x=BatchNormalization()(x)
        x=Add()([x,inputs])
    x=Activation(activation)(x)
    return x

class Self_Attention(Layer):

    def __init__(self, nb_head, size_per_head, **kwargs):
        self.nb_head = nb_head
        self.size_per_head = size_per_head
        self.output_dim = nb_head*size_per_head
        super(Self_Attention, self).__init__(**kwargs)

    def build(self, input_shape):
        self.WQ = self.add_weight(name='WQ', 
                                  shape=(input_shape[0][-1], self.output_dim),
                                  initializer='glorot_uniform',
                                  trainable=True)
        self.WK = self.add_weight(name='WK', 
                                  shape=(input_shape[1][-1], self.output_dim),
                                  initializer='glorot_uniform',
                                  trainable=True)
        self.WV = self.add_weight(name='WV', 
                                  shape=(input_shape[2][-1], self.output_dim),
                                  initializer='glorot_uniform',
                                  trainable=True)
        super(Self_Attention, self).build(input_shape)
        
    def Mask(self, inputs, seq_len, mode='mul'):
        if seq_len == None:
            return inputs
        else:
            mask = K.one_hot(seq_len[:,0], K.shape(inputs)[1])
            mask = 1 - K.cumsum(mask, 1)
            for _ in range(len(inputs.shape)-2):
                mask = K.expand_dims(mask, 2)
            if mode == 'mul':
                return inputs * mask
            if mode == 'add':
                return inputs - (1 - mask) * 1e12
                
    def call(self, x):

        if len(x) == 3:
            Q_seq,K_seq,V_seq = x
            Q_len,V_len = None,None
        elif len(x) == 5:
            Q_seq,K_seq,V_seq,Q_len,V_len = x

        Q_seq = K.dot(Q_seq, self.WQ)
        Q_seq = K.reshape(Q_seq, (-1, K.shape(Q_seq)[1], self.nb_head, self.size_per_head))
        Q_seq = K.permute_dimensions(Q_seq, (0,2,1,3))
        K_seq = K.dot(K_seq, self.WK)
        K_seq = K.reshape(K_seq, (-1, K.shape(K_seq)[1], self.nb_head, self.size_per_head))
        K_seq = K.permute_dimensions(K_seq, (0,2,1,3))
        V_seq = K.dot(V_seq, self.WV)
        V_seq = K.reshape(V_seq, (-1, K.shape(V_seq)[1], self.nb_head, self.size_per_head))
        V_seq = K.permute_dimensions(V_seq, (0,2,1,3))

        A = K.batch_dot(Q_seq, K_seq, axes=[3,3])/(self.size_per_head**0.5)
        A = K.permute_dimensions(A, (0,3,2,1))
        A = self.Mask(A, V_len, 'add')
        A = K.permute_dimensions(A, (0,3,2,1))    
        A = K.softmax(A)

        O_seq = K.batch_dot(A, V_seq, axes=[3,2])
        O_seq = K.permute_dimensions(O_seq, (0,2,1,3))
        O_seq = K.reshape(O_seq, (-1, K.shape(O_seq)[1], self.output_dim))
        O_seq = self.Mask(O_seq, Q_len, 'mul')
        return O_seq
        
    def compute_output_shape(self, input_shape):
        return (input_shape[0][0], input_shape[0][1], self.output_dim)

from keras.engine.network import Layer
from keras.layers import InputSpec
import keras.backend as K


class Conv_attention_layer(Layer):

    def __init__(self, ch, **kwargs):
        super(Conv_attention_layer, self).__init__(**kwargs)
        self.channels = ch
        self.filters_f_g = self.channels // 8
        self.filters_h = self.channels

    def build(self, input_shape):
        kernel_shape_f_g = (1, 1) + (self.channels, self.filters_f_g)
        kernel_shape_h = (1, 1) + (self.channels, self.filters_h)

        # Create a trainable weight variable for this layer:
        self.gamma = self.add_weight(name='gamma', shape=[1], initializer='zeros', trainable=True)
        self.kernel_f = self.add_weight(shape=kernel_shape_f_g,
                                        initializer='glorot_uniform',
                                        name='kernel_f',
                                        trainable=True)
        self.kernel_g = self.add_weight(shape=kernel_shape_f_g,
                                        initializer='glorot_uniform',
                                        name='kernel_g',
                                        trainable=True)
        self.kernel_h = self.add_weight(shape=kernel_shape_h,
                                        initializer='glorot_uniform',
                                        name='kernel_h',
                                        trainable=True)

        super(Conv_attention_layer, self).build(input_shape)
        # Set input spec.
        self.input_spec = InputSpec(ndim=4,
                                    axes={3: input_shape[-1]})
        self.built = True

    def call(self, x):
        def hw_flatten(x):
            return K.reshape(x, shape=[K.shape(x)[0], K.shape(x)[1]*K.shape(x)[2], K.shape(x)[3]])

        f = K.conv2d(x,
                     kernel=self.kernel_f,
                     strides=(1, 1), padding='same')  # [bs, h, w, c']
        g = K.conv2d(x,
                     kernel=self.kernel_g,
                     strides=(1, 1), padding='same')  # [bs, h, w, c']
        h = K.conv2d(x,
                     kernel=self.kernel_h,
                     strides=(1, 1), padding='same')  # [bs, h, w, c]

        s = K.batch_dot(hw_flatten(g), K.permute_dimensions(hw_flatten(f), (0, 2, 1)))  # # [bs, N, N]

        beta = K.softmax(s, axis=-1)  # attention map

        o = K.batch_dot(beta, hw_flatten(h))  # [bs, N, C]

        o = K.reshape(o, shape=K.shape(x))  # [bs, h, w, C]
        x = self.gamma * o + x

        return x

    def compute_output_shape(self, input_shape):
        return input_shape





if tf.__version__ >= '2.0':
    from tensorflow.keras.layers import Dense, Input,Layer,Conv2D,Flatten,MaxPooling2D
else:
    from keras.layers import Dense, Input,Layer,Conv2D,Flatten,MaxPooling2D
    import keras
    
import numpy as np


# Multiple Centers 
class Probability_CLF_Mul(Layer):
    """docstring for Probability_CLF"""
    def __init__(self, output_dim,num_centers=1,non_trainable=0,activation=None,**kwargs):
        self.output_dim = output_dim
        self.num_centers = num_centers
        self.non_trainable = non_trainable
        self.activation = activation
        super(Probability_CLF_Mul, self).__init__(**kwargs)

    def build(self,input_shape):
        self.centers = {}
        #self.kernels = []
        
        for idx in range(self.output_dim):
            self.centers[idx] = []
            
            
            if idx in range(self.non_trainable):
                trainable = False
            else:
                trainable = True
            for c in range(self.num_centers):
                W = self.add_weight(name='center%d_%d'%(idx,c),shape=(input_shape[1],),initializer='uniform',trainable=True)
                self.centers[idx].append(W)
            
        super(Probability_CLF_Mul,self).build(input_shape)      

    def call(self,x,training=None):
        logits = []
        re_logits = []
        # Fixed Sigma
        sigma = 1.0
        #W = keras.activations.softmax(self.kernel)
        for idx in range(self.output_dim):

            G = []
            for c in range(self.num_centers):

                G.append(self.gaussian_activation(tf.math.squared_difference(x ,self.centers[idx][c]),sigma))
    
            G = tf.stack(G,axis=1)
            
            
            P = tf.reduce_sum(G,axis=1) / (tf.reduce_sum(G,axis=1) + self.num_centers - tf.reduce_max(G,axis=1) * self.num_centers )
            
            logits.append(P)

            
        
        logits = tf.stack(logits,axis=1)
        re_logits = logits
        if self.activation is not None:
            re_logits = self.activation(logits)
        

        return logits



    def gaussian_activation(self,x,sigma=None):
        sigma = 1. if sigma == None else sigma
        return tf.exp(-tf.reduce_sum(x,axis=1) / (2. * sigma * sigma)) 




    def compute_output_shape(self,input_shape):
        return (input_shape[0],self.output_dim)