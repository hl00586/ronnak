class Z_Layer(object):
    
    def __init__(self,
                 n_in,                   # input size
                 n_hidden = 30,               # Number of units in the layer
                 state_is_tuple = True,  
                 activation = tanh,
                 initializer = tf.random_uniform_initializer(-0.05, 0.05,
                                                             seed = 2345)
                ):
        
        '''
        Zlayer
        Inputs
        ------
             inputs constructor:
             num_units = number of output units
             activation = activation
            
        Tensorflow Edition
        '''
        self._n_hidden = n_hidden
        self._n_in = n_in
        self._activation = activation
        self._idx = 'ZLayer'
        
        with vs.variable_scope('ZLayerWeights') as var_scope: 
            w1 = tf.get_variable('W1', [n_in,1], dtype = tf.float32, 
                                 initializer = initializer)
            w2 = tf.get_variable('W2', [n_hidden,1], dtype = tf.float32, 
                                 initializer = initializer)
            bias = tf.get_variable('Bias', 
                                   [1], 
                                   initializer=tf.constant_initializer(0.0), 
                                   dtype = tf.float32)
            
        self.rlayer = RCNNCell(self._n_hidden, idx = self._idx, 
                               initializer = initializer )
        
        if not state_is_tuple:
            print 'Please use the tuple function. Not implemented for usage with tensors.'
            raise NotImplemented
        
    
    
    def forward(self, x_t, z_t, z_tm1, h_tm1):
        
        
        with vs.variable_scope('ZLayerWeights', reuse=True) as var_scope:
            w1 = tf.get_variable('W1', dtype = tf.float32)
            w2 = tf.get_variable('W2', dtype = tf.float32)
            bias = tf.get_variable('Bias', dtype = tf.float32)

            
        # make a prediction
        pz_t = sigmoid(
                    tf.matmul(x_t, w1) +
                    tf.matmul(h_tm1[:,-self.n_hidden:], w2) +
                    bias, name = 'pzt_sigmoid'
                )
        logits = tf.matmul(x_t, w1) + \
                    tf.matmul(h_tm1[:,-self.n_hidden:], w2) +\
                    bias
        
        xz_t =  tf.concat(1, [x_t, tf.reshape(z_t, [-1,1])], name = 'xzt_concat')
        
        rnn_outputs, final_state = self.rlayer( xz_t,  h_tm1)
        
        return final_state, pz_t, logits
    
    
    def forward_all(self, x, z):
        
        assert len(x.get_shape()) == 3
        assert len(z.get_shape()) == 2
        
        # get the variables
        with vs.variable_scope('ZLayerWeights', reuse=True) as var_scope:
            w1 = tf.get_variable('W1', dtype = tf.float32)
            w2 = tf.get_variable('W2', dtype = tf.float32)
            bias = tf.get_variable('Bias', dtype = tf.float32)

        
        # z would be ( len, batch_size)
        # x would be (len, batch_size, n_d)
        xz = tf.concat(2, [x, tf.expand_dims(z, 2)])
        
        # initial state
        h0 = tf.zeros([1, x.get_shape()[1], self._n_hidden], 
                       name = 'H_0_matrix_zlayer', 
                       dtype = tf.float32)
        
        # get the zero state for the rlayer
        h_temp = self.rlayer.zero_state(x.get_shape()[1]) 
        
        # ensure that the variables are reused in RCNN
        self.rlayer.reuse = True
        
        
        with tf.variable_scope('RNN'):
            
            # here too changed the dynamic rnn to scan
            htp = tf.scan(self.rlayer, xz, initializer= h_temp)
            if len(htp.get_shape())>1:
            
                h = htp[:,:, self.rlayer._order * self.rlayer._num_units:]
            else:
                h = htp[:, self.rlayer._order * self.rlayer._num_units:]
        

        h_prev = tf.concat(0,[h0, h[:-1]])
        
        # check shapes
        assert len(h_prev.get_shape()) == 3
        assert len(h.get_shape())      == 3

        # get the shapes
        xshape = x.get_shape().as_list()
        hshape = h_prev.get_shape().as_list()
        
        # reshape x such that matmul is possible
        tp1 = tf.reshape(x, [-1, xshape[2]])
        a_tp = tf.matmul(tp1, w1)
        a = tf.reshape(a_tp, [-1, xshape[1], 1])
        
        # reshape h_prev such that matmul is possible
        tp2 = tf.reshape(h_prev, [-1, hshape[2]])
        b_tp = tf.matmul(tp2, w2)
        b = tf.reshape(b_tp, [-1, hshape[1], 1])
        
        # sigmoid it
        logits = a+b+bias
        pz = sigmoid(logits)
        
        # reshape, remove last dim
        pz_reshaped = tf.squeeze(pz, squeeze_dims= [2])
        logits_reshaped = tf.squeeze(logits, squeeze_dims= [2])
        
        # return the matrix of predictions
        assert len(pz_reshaped.get_shape()) == 2
        assert len(logits_reshaped.get_shape()) == 2
        
        return pz_reshaped, logits_reshaped
    
        
    # adapted version of sample
    def sample(self, prev, x_t, seed = 2345):
        
        # get the appropriate values
        h_tm1 = prev[0]
        z_tm1 =prev[1]
    
        # get the variables
        with vs.variable_scope('ZLayerWeights', reuse=True) as var_scope:
            w1 = tf.get_variable('W1', dtype = tf.float32)
            w2 = tf.get_variable('W2', dtype = tf.float32)
            bias = tf.get_variable('Bias', dtype = tf.float32)
        
        
        pz_t = sigmoid(
                    tf.matmul(x_t, w1) +
                    tf.matmul(h_tm1[:,-self._n_hidden:], w2) +
                    bias
                )
        
        
        pz_t = tf.squeeze(pz_t)
        
        # predict z
        z_t = tf.cast(tf.less_equal(tf.random_uniform(pz_t.get_shape(),
                                                      dtype=tf.float32, seed=seed),
                                                        pz_t),
                                                      tf.float32)
        
        xz_t = tf.concat(1,[x_t, tf.reshape(z_t,  [-1,1])])
        
        # set reuse in rlayer to none
        self.rlayer.reuse = None
        
       
        
        # do a forward pass
        h_t = self.rlayer(h_tm1, xz_t, scope = 'RNN/RCNN_cell_ZLayer',
                          scope2 = 'RNN/RCNN_Feed_Forward_Layer')
        
        return [h_t , tf.expand_dims(z_t, 1)] 
    
    def sample_all(self, x):
        
        # get the variables
        with vs.variable_scope('ZLayerWeights', reuse=True) as var_scope:
            w1 = tf.get_variable('W1', dtype = tf.float32)
            w2 = tf.get_variable('W2', dtype = tf.float32)
            bias = tf.get_variable('Bias', dtype = tf.float32)
        
        h0 = tf.zeros((x.get_shape()[1], self._n_hidden*(self.rlayer._order+1)), dtype = tf.float32)
        z0 = tf.zeros((x.get_shape()[1],), dtype=tf.float32)

        
        h, z = tf.scan(
                    self.sample,
                    x, 
                    initializer = [ h0, tf.expand_dims(z0, 1 )]
                    )
        
        z = tf.squeeze(z, squeeze_dims = [2])
        assert len(z.get_shape()) == 2
        
        return z
